# hawkbit-info

hawkbit-info ist in C++ geschriebenes Programm, welches unter anderem  gloox und
boost verwendet. gloox ist eine C++ Bibliothek für XMPP.

Das Programm gibt einen Informationen zu einem XMPP Server, sowie Informationen
zum aktuellen Account.

* XEP-0030: Service Discovery
* XEP-0048: Bookmark Storage

Nach der Angabe von JID und das Passwort, verbindet sich das Programm zum Server
und meldet den Benutzer an. Danach kann der Anwender durch Befehle Information
vom Server und Account abfragen.

# API

* https://pages.codeberg.org/xmpp-messenger/hawkbit-info

# Beispiel

```
% ./hawkbit-info     
JID: hawkbit@domain.tld
Passwort: 
Connect...
Zertifikat
certificate: OK
Resource bind: HawkbitInfo OK
Server connected: OK
XMPP> help
bookmarks
xep
discover
exit
XMPP> xep
Discovering Information About a Jabber Entity
Identity Prosody (im)server
Identity Prosody (pep)pubsub
XEP-0092: OK
XEP-0050: OK
XEP-0021: OK
XEP-0030: OK
urn:xmpp:blocking: OK
XEP-0049: OK
XEP-0199: OK
XEP-0060: #publish OK
XEP-0077: OK
XEP-0202: OK
XEP-0160: OK
jabber:iq:roster: OK
XEP-0054: OK
Discovering the Items Associated with a Jabber Entity: domain.tld
Item  (proxy.domain.tld)
Item  (conference.domain.tld)
Item  (search.domain.tld)
Item  (upload.domain.tld)
Item  (pubsub.domain.tld)
XMPP> discover
discoveringService
5
Discovering the Items Associated with a Jabber Entity: proxy.domain.tld
Discovering the Items Associated with a Jabber Entity: conference.domain.tld
Item room1 (room1@conference.domain.tld)
Item room2 (room2@conference.domain.tld)
Item room3 (room3@conference.domain.tld)
Discovering the Items Associated with a Jabber Entity: search.domain.tld
Discovering the Items Associated with a Jabber Entity: uploads.domain.tld
Discovering the Items Associated with a Jabber Entity: pubsub.domain.tld
Item  (pubsub.domain.tld)rss_abc3
Item  (pubsub.domain.tld)rss_abc2
Item  (pubsub.domain.tld)rss_abc1
XMPP> exit
```
