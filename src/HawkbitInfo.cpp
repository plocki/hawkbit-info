#include "HawkbitInfo.hpp"

#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
// unixtime->date
#include <boost/date_time/gregorian/gregorian.hpp>

#include "iostream"

using namespace gloox;

/*!
 *
 * Für die JID wird diese mit user@domain.tld/resource erstellt (full JID).
 *
 * <pre>
 *  The term "full JID" refers to an XMPP address of the form
 *  <localpart@domainpart/resourcepart> (for a particular authorized
 *  client or device associated with an account) or of the form
 *  <domainpart/resourcepart> (for a particular resource or script
 *  associated with a server).
 * </pre>
 *
 * https://tools.ietf.org/html/rfc6120#section-1.4
 *
 * @return - Aktuell immer true
 *
 */
bool HawkbitInfo::setup(std::string user, std::string host,
                        std::string resource, std::string password) {
  m_user = user;
  m_host = host;
  std::string id(user + '@' + host + '/' + resource);
  JID jid(id);
  j = new Client(jid, password);
  j->disco()->setVersion(HAWKBIT_INFO_NAME, HAWKBIT_INFO_VERSION);
  j->disco()->setIdentity("client", HAWKBIT_INFO_NAME);
  j->registerConnectionListener(this);
  j->registerMessageHandler(this);
  j->logInstance().registerLogHandler(LogLevel::LogLevelDebug,
                                      LogArea::LogAreaAll, this);
  return true;
}

/*!
 * Verbindung zu XMPP Server aufbauen.
 *
 */
void HawkbitInfo::connect() {
  std::cout << "Connecting..." << std::endl;
  j->connect();
  // j->send(IQ(IQ::IqType::Get, JID(m_host), XMLNS_VERSION));
}

void HawkbitInfo::checkXEPs() {
  Disco *disco = j->disco();
  disco->getDiscoInfo(JID(m_host), "", this, 0);
  disco->getDiscoItems(JID(m_host), "", this, 0);
}

void HawkbitInfo::discoveringService() {
  Disco *disco = j->disco();
  std::cout << "discoveringService" << std::endl;
  std::cout << m_discoitems.size() << std::endl;
  for (std::list<std::string>::iterator it = m_discoitems.begin();
       it != m_discoitems.end(); ++it) {
    disco->getDiscoItems(JID(*it), "", this, 0);
  }
}

void HawkbitInfo::retrieveBookmarks() {
  BookmarkStorage *bs = new BookmarkStorage(j);
  bs->registerBookmarkHandler(this);
  bs->requestBookmarks();
}

MUCRoom *HawkbitInfo::join(std::string mucroom) {
  JID roomJID(mucroom);
  MUCRoom *m_room = new MUCRoom(j, roomJID, this, 0);
  BOOST_LOG_TRIVIAL(info) << "Join room: " << mucroom;
  m_room->join();
  return m_room;
}

/*!
 * Verbindung mit dem XMPP Server trennen.
 *
 */

void HawkbitInfo::disconnect() { j->disconnect(); }

void HawkbitInfo::handleMessageSession(MessageSession *session) {
  BOOST_LOG_TRIVIAL(info) << "handleMessageSession";
}

void HawkbitInfo::handleMessage(const Message &stanza,
                                MessageSession *session = 0) {
  BOOST_LOG_TRIVIAL(info) << "Message: " << stanza.id() << " "
                          << stanza.xmlLang();
}

void HawkbitInfo::handleMessageEvent(const JID &from, MessageEventType event) {
  BOOST_LOG_TRIVIAL(info) << "Message: ";
}

/* ================================================================ */
/*  ConnectionListener                                              */
/* ================================================================ */

bool HawkbitInfo::onTLSConnect(const CertInfo &certinfo) {
  BOOST_LOG_TRIVIAL(info) << "onTLSConnect";
  BOOST_LOG_TRIVIAL(info) << "Status: " << certinfo.status;
  BOOST_LOG_TRIVIAL(info) << "Issuer: " << certinfo.issuer.c_str();
  BOOST_LOG_TRIVIAL(info) << "Server: " << certinfo.server.c_str();
  BOOST_LOG_TRIVIAL(info) << "Protocol: " << certinfo.protocol.c_str();
  BOOST_LOG_TRIVIAL(info) << "MAC: " << certinfo.mac.c_str();
  BOOST_LOG_TRIVIAL(info) << "Cipher: " << certinfo.cipher.c_str();
  BOOST_LOG_TRIVIAL(info) << "Compression: " << certinfo.compression.c_str();
  BOOST_LOG_TRIVIAL(info) << "Date from: " << boost::posix_time::from_time_t(certinfo.date_from).date();
  BOOST_LOG_TRIVIAL(info) << "Date to: " << boost::posix_time::from_time_t(certinfo.date_to).date();

  if (certinfo.status == CertStatus::CertOk) {
    std::cout << "Certificate: " << OK << std::endl;
  } else {
    std::cout << "Certificate: " << KO << std::endl;
  }
  return true;
}

void HawkbitInfo::onResourceBind(const std::string &resource) {
  std::cout << "Resource bind: " << resource << " " << OK << std::endl;
}

void HawkbitInfo::onResourceBindError(const Error *error) {
  BOOST_LOG_TRIVIAL(error) << "SessionCreateError:" << error;
}

void HawkbitInfo::onConnect() {
  std::cout << "Server connected: " << OK << std::endl;
  BOOST_LOG_TRIVIAL(info) << "onConnect";
}

void HawkbitInfo::onDisconnect(ConnectionError e) {
  BOOST_LOG_TRIVIAL(error) << "onDisconnect:" << e;
  BOOST_LOG_TRIVIAL(error) << this->j->streamError();
}

void HawkbitInfo::onSessionCreateError(const Error *error) {
  BOOST_LOG_TRIVIAL(error) << "SessionCreateError:" << error;
}

/* ================================================================ */
/*! \page xpe0030 XEP-0030: Service Discovery
 * DiscoHandler
 *
 * <ul>
 * <li>https://xmpp.org/extensions/xep-0030.html</li>
 * <li>https://xmpp.org/registrar/disco-features.html</li>
 * </ul>
 */
/* ================================================================ */

/*! \brief Verarbeiten der DiscoInfo
 *         Diese Methode ist ein Callback von gloox für die Verarbeitung von
 * DiscoInfo.
 *
 * <ul>
 * <li>Identity<li>
 * <li>Features</li>
 * </ul>
 *
 * @param from
 * @param info
 * @param context
 *
 *
 */
void HawkbitInfo::handleDiscoInfo(const JID &from, const Disco::Info &info,
                                  int context) {
  std::cout << "Discovering Information About a Jabber Entity" << std::endl;

  std::list<Disco::Identity *> identities = info.identities();
  for (std::list<Disco::Identity *>::iterator it = identities.begin();
       it != identities.end(); ++it) {
    std::cout << "Identity " << (*it)->name() << " (" << (*it)->type() << ")"
              << (*it)->category() << std::endl;
  }

  std::list<std::string> features = info.features();
  // XEP-0092: Software Version
  if (info.hasFeature(XMLNS_VERSION)) {
    std::cout << "XEP-0092: " << OK << std::endl;
  } else {
    std::cout << "XEP-0092: " << KO << std::endl;
  }
  // XEP-0050 -Ad-Hoc Commands
  if (info.hasFeature(XMLNS_ADHOC_COMMANDS)) {
    std::cout << "XEP-0050: " << OK << std::endl;
  } else {
    std::cout << "XEP-0050: " << KO << std::endl;
  }
  // XEP-0012: Last Activity
  if (info.hasFeature(XMLNS_LAST)) {
    std::cout << "XEP-0021: " << OK << std::endl;
  } else {
    std::cout << "XEP-0021: " << KO << std::endl;
  }
  // XEP-0030: Service Discovery
  if (info.hasFeature(XMLNS_DISCO_INFO)) {
    std::cout << "XEP-0030: " << OK << std::endl;
  } else {
    std::cout << "XEP-0030: " << KO << std::endl;
  }
  //
  if (info.hasFeature("urn:xmpp:blocking")) {
    std::cout << "urn:xmpp:blocking: " << OK << std::endl;
  } else {
    std::cout << "urn:xmpp:blocking: " << KO << std::endl;
  }
  // XEP-0049: Private XML Storage
  if (info.hasFeature(XMLNS_PRIVATE_XML)) {
    std::cout << "XEP-0049: " << OK << std::endl;
  } else {
    std::cout << "XEP-0049: " << KO << std::endl;
  }
  //  XEP-0199: XMPP Ping
  if (info.hasFeature(XMLNS_XMPP_PING)) {
    std::cout << "XEP-0199: " << OK << std::endl;
  } else {
    std::cout << "XEP-0199: " << KO << std::endl;
  }
  // XEP-0060 Publishing items is supported.
  if (info.hasFeature("http://jabber.org/protocol/pubsub#publish")) {
    std::cout << "XEP-0060: #publish " << OK << std::endl;
  } else {
    std::cout << "XEP-0060: #publish" << KO << std::endl;
  }
  // XEP-0077: In-Band Registration
  if (info.hasFeature(XMLNS_REGISTER)) {
    std::cout << "XEP-0077: " << OK << std::endl;
  } else {
    std::cout << "XEP-0077: " << KO << std::endl;
  }
  // XEP-0202: Entity Time
  if (info.hasFeature("urn:xmpp:time")) {
    std::cout << "XEP-0202: " << OK << std::endl;
  } else {
    std::cout << "XEP-0202: " << KO << std::endl;
  }
  // XEP-0160: Best Practices for Handling Offline Messages
  if (info.hasFeature("msgoffline")) {
    std::cout << "XEP-0160: " << OK << std::endl;
  } else {
    std::cout << "XEP-0160: " << KO << std::endl;
  }
  //
  if (info.hasFeature(XMLNS_ROSTER)) {
    std::cout << "jabber:iq:roster: " << OK << std::endl;
  } else {
    std::cout << "jabber:iq:roster: " << KO << std::endl;
  }
  // XEP-0054: vcard-temp
  if (info.hasFeature(XMLNS_VCARD_TEMP)) {
    std::cout << "XEP-0054: " << OK << std::endl;
  } else {
    std::cout << "XEP-0054: " << KO << std::endl;
  }
}

/*! \brief Verarbeiten der DiscoItems
 *         Diese Methode ist ein Callback von gloox für die Verarbeitung von
 * DiscoItems.
 *
 * @param from
 * @param items
 * @param context
 *
 *
 */
void HawkbitInfo::handleDiscoItems(const JID &from, const Disco::Items &items,
                                   int context) {
  std::cout << "Discovering the Items Associated with a Jabber Entity: "
            << from.full() << std::endl;
  BOOST_LOG_TRIVIAL(info) << items.node();

  std::list<Disco::Item *> itemslist = items.items();
  for (std::list<Disco::Item *>::iterator it = itemslist.begin();
       it != itemslist.end(); ++it) {
    std::cout << "Item " << (*it)->name() << " (" << (*it)->jid().full() << ")"
              << (*it)->node() << std::endl;
    m_discoitems.push_back(std::string((*it)->jid().full()));
  }
}

void HawkbitInfo::handleDiscoError(const JID &from, const Error *error,
                                   int context) {}

bool HawkbitInfo::handleDiscoSet(const IQ &iq) { return false; }

/* ================================================================ */
/*! \page xep0048 XEP-0048: Bookmark Storage
 *
 * <ul>
 * <li>https://xmpp.org/extensions/xep-0048.html</li>
 * <li>https://camaya.net/api/gloox-1.0.23/classgloox_1_1BookmarkHandler.html</li>
 * </ul>
 */
/* ================================================================ */

/*! \brief Verarbeiten der Bookmarks.
 *         Diese Methode ist ein Callback von gloox für die Verarbeitung von
 * Bookmarks.
 *
 * @param bList
 * @param cList
 *
 */
void HawkbitInfo::handleBookmarks(const BookmarkList &bList,
                                  const ConferenceList &cList) {

  std::list<BookmarkListItem> list1 = bList;
  for (std::list<BookmarkListItem>::iterator it = list1.begin();
       it != list1.end(); ++it) {
    std::cout << "Bookmark: " << (*it).name << std::endl;
  }

  std::list<ConferenceListItem> list2 = cList;
  for (std::list<ConferenceListItem>::iterator it = list2.begin();
       it != list2.end(); ++it) {
    std::cout << "Conference:: " << (*it).name << std::endl;
  }
}

void HawkbitInfo::handleMUCParticipantPresence(
    MUCRoom *room, const MUCRoomParticipant participant,
    const Presence &presence) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCParticipantPresence";
}

void HawkbitInfo::handleMUCMessage(MUCRoom *room, const Message &msg,
                                   bool priv) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCMessage: " << msg.body();
}

bool HawkbitInfo::handleMUCRoomCreation(MUCRoom *room) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCRoomCreation";
  return handleMUCRoomCreation(room);
}

void HawkbitInfo::handleMUCSubject(MUCRoom *room, const std::string &nick,
                                   const std::string &subject) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCSubject";
}

void HawkbitInfo::handleMUCInviteDecline(MUCRoom *room, const JID &invitee,
                                         const std::string &reason) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCInviteDecline";
}

void HawkbitInfo::handleMUCError(MUCRoom *room, StanzaError error) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCError" << room->name() << " " << error;
}

void HawkbitInfo::handleMUCInfo(MUCRoom *room, int features,
                                const std::string &name,
                                const DataForm *infoForm) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCInfo";
}

void HawkbitInfo::handleMUCItems(MUCRoom *room, const Disco::ItemList &items) {
  BOOST_LOG_TRIVIAL(info) << "handleMUCItems";
}

void HawkbitInfo::handleLog(LogLevel level, LogArea area,
                            const std::string &message) {
  if (level == LogLevelDebug) {
    BOOST_LOG_TRIVIAL(debug) << message;
  } else if (level == LogLevelWarning) {
    BOOST_LOG_TRIVIAL(debug) << message;
  } else if (level == LogLevelError) {
    BOOST_LOG_TRIVIAL(error) << message;
  }
}
