/*! \mainpage hawkbit-info
 * \tableofcontents
 * \section into Einleitung
 * Hawkbit-info ist ein C++ Programm um XMPP und die Bibliothek
 * <a href="https://camaya.net/gloox/">gloox</a> zu lernen und zu verstehen.
 *
 * Das Programm gibt unter Angabe von JID und das Passwort einige Information zu
 * dem Server und zum Account zurück. Das Programm ist Shell ähnlich aufgebaut,
 * der Anwender interagiert mit dem Programm über Befehle.
 *
 * \section basics Grundlagen
 *
 * - Quellcode: https://codeberg.org/xmpp-messenger/hawkbit-info
 * - Dokumentation:
 * https://pages.codeberg.org/xmpp-messenger/hawkbit-info/index.html
 *
 * \section gloox gloox
 *
 * API: https://camaya.net/api/gloox-1.0.23/
 *
 * \section beispiel Beispiel
 * <pre>
% ./hawkbit-info
JID: hawkbit@domain.tld
Passwort:
Connect...
Zertifikat
certificate: OK
Resource bind: HawkbitInfo OK
Server connected: OK
XMPP> help
bookmarks
xep
discover
exit
XMPP> xep
Discovering Information About a Jabber Entity
Identity Prosody (im)server
Identity Prosody (pep)pubsub
XEP-0092: OK
XEP-0050: OK
XEP-0021: OK
XEP-0030: OK
urn:xmpp:blocking: OK
XEP-0049: OK
XEP-0199: OK
XEP-0060: #publish OK
XEP-0077: OK
XEP-0202: OK
XEP-0160: OK
jabber:iq:roster: OK
XEP-0054: OK
Discovering the Items Associated with a Jabber Entity: domain.tld
Item  (proxy.domain.tld)
Item  (conference.domain.tld)
Item  (search.domain.tld)
Item  (upload.domain.tld)
Item  (pubsub.domain.tld)
XMPP> discover
discoveringService
5
Discovering the Items Associated with a Jabber Entity: proxy.domain.tld
Discovering the Items Associated with a Jabber Entity: conference.domain.tld
Item room1 (room1@conference.domain.tld)
Item room2 (room2@conference.domain.tld)
Item room3 (room3@conference.domain.tld)
Discovering the Items Associated with a Jabber Entity: search.domain.tld
Discovering the Items Associated with a Jabber Entity: uploads.domain.tld
Discovering the Items Associated with a Jabber Entity: pubsub.domain.tld
Item  (pubsub.domain.tld)rss_abc3
Item  (pubsub.domain.tld)rss_abc2
Item  (pubsub.domain.tld)rss_abc1
XMPP> exit
 * </pre>
 *
 */

#include "HawkbitInfo.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <iostream>
#include <readline/readline.h>
#include <stdlib.h>
#include <thread>

/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */
void run_session(HawkbitInfo *session) { session->connect(); }

/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

void init() {
  logging::add_file_log(
      keywords::file_name = "hawkbitinfo_%N.log", /*< file name pattern >*/
      keywords::rotation_size =
          10 * 1024 * 1024, /*< rotate files every 10 MiB... >*/
      keywords::time_based_rotation = sinks::file::rotation_at_time_point(
          0, 0, 0),                                  /*< ...or at midnight >*/
      keywords::format = "[%TimeStamp%]: %Message%", /*< log record format >*/
      keywords::auto_flush = true);

  logging::add_console_log(std::cout,
                           boost::log::keywords::format = ">> %Message%");

  logging::core::get()->set_filter(logging::trivial::severity >=
                                   logging::trivial::info);
}

/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */
int main(int argc, char *argv[]) {

  // Setup logging Framework

  // Setup logging Framework
  init();
  logging::add_common_attributes();
  using namespace logging::trivial;
  src::severity_logger<severity_level> lg;

  // Programm mit Version in Log und auf der Console ausgeben
  BOOST_LOG_TRIVIAL(info) << "Starting " << HAWKBIT_INFO_NAME
                          << " Version: " HAWKBIT_INFO_VERSION;
  std::cout << "Starting " << HAWKBIT_INFO_NAME
            << " Version: " HAWKBIT_INFO_VERSION << std::endl;

  // JID vom Benutzer erfassen lassen
  std::string jid;
  std::cout << "JID: ";
  std::cin >> jid;

  // Aufteilen der JID user@host in zwei variablen
  std::vector<std::string> strs;
  boost::split(strs, jid, boost::is_any_of("@"));
  std::string user = strs.at(0);
  std::string host = strs.at(1);

  // Passwort vom Benutzer erfassen lassen
  char *pwd = getpass("Passwort: ");

  // Hawkbit Info erzeugen und einrichten
  HawkbitInfo *hawkbitInfo = new HawkbitInfo();
  hawkbitInfo->setup(user, host, "HawkbitInfo", pwd);

  // HawkbitInfo läuft in einem eigenen Thread
  std::thread session_thread(run_session, hawkbitInfo);

  // In einer Schleife die Befehle vom Benutzer abfragen
  // und die gewünschte Aktion ausführen.
  while (true) {
    // Damit die Befehle vom HawkbitInfo im Thread bearbeitet werden können
    sleep(2);
    char *cmd = readline("XMPP> ");
    if (strcmp(cmd, "help") == 0) {
      // Die Hilfe ausgeben
      std::cout << "bookmarks" << std::endl;
      std::cout << "xep" << std::endl;
      std::cout << "discover" << std::endl;
      std::cout << "exit" << std::endl;
    } else if (strcmp(cmd, "bookmarks") == 0) {
      // Bookmarks lesen
      hawkbitInfo->retrieveBookmarks();
    } else if (strcmp(cmd, "xep") == 0) {
      // XEPs (Features vom Server ermitteln
      hawkbitInfo->checkXEPs();
    } else if (strcmp(cmd, "discover") == 0) {
      hawkbitInfo->discoveringService();
    } else if (strcmp(cmd, "exit") == 0) {
      // Programm beenden
      break;
    } else {
      // Der Befehl ist unbekannt
      std::cout << "Befehl unbekannt: help für Hife" << std::endl;
    }
  }
  // Das Programm wird beendet, wir trennen die Verbindung
  hawkbitInfo->disconnect();
  sleep(5);
  session_thread.join();
  return EXIT_SUCCESS;
}
