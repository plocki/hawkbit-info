/*! \page whatIsGlooxPage Was ist gloox?
 *
 * gloox ist eine C++ XMPP lib:
 * https://camaya.net/gloox/
 * https://camaya.net/api/gloox-1.0.22/
 *
 * Diese lib wird in XmppClientSession verwendet.
 */

#ifndef XMPP_HAWKBIT_INFO_H___
#define XMPP_HAWKBIT_INFO_H___

#define HAWKBIT_INFO_NAME "INFO"
#define HAWKBIT_INFO_VERSION "0.0.0"

#define OK "\033[1;32mOK\033[0m"
#define KO "\033[1;31mKO\033[0m"

#include <gloox/bookmarkstorage.h>
#include <gloox/chatstatehandler.h>
#include <gloox/client.h>
#include <gloox/connectionlistener.h>
#include <gloox/disco.h>
#include <gloox/discohandler.h>
#include <gloox/message.h>
#include <gloox/messageeventhandler.h>
#include <gloox/messagehandler.h>
#include <gloox/messagesessionhandler.h>
#include <gloox/mucroom.h>
#include <gloox/rostermanager.h>

using namespace gloox;

class HawkbitInfo : public MessageHandler,
                    ConnectionListener,
                    MessageEventHandler,
                    MessageSessionHandler,
                    MUCRoomHandler,
                    DiscoHandler,
                    LogHandler,
                    BookmarkHandler {

public:
  /*! \brief Setup für Hawkbit-Info.
   *         Setzt die Login-Daten und erstellt den gloox Client.
   *
   * Diese Methode wird verwenden um Hawkbit-Info von der main-Funktion zu
   * initialisieren.
   *
   * @param user - Username des XMPP Account - localpart
   * @param host - Hostname des XMPP Account - domain
   * @param resource - XMPP resource
   * @param password - Passwort des XMPP Account
   *
   * @return - Muss ich mir noch überlegen
   *
   */
  bool setup(std::string user, std::string host, std::string resource,
             std::string password);

  /*! \brief Verbindung mit XMPP Server herstellten.
   *         In dieser Methode wir die Verbindung mit dem XMPP Server
   *         hergestellt.
   *
   * Diese Methode wird verwenden um Hawkbit-Info von der main-Funktion zu
   * initialisieren.
   *
   */
  void connect();
  void checkXEPs();
  void discoveringService();
  void retrieveBookmarks();

  MUCRoom *join(std::string mucroom);
  void disconnect();

  /* ================================================================ */
  /*  ConnectionListener                                              */
  /* ================================================================ */

  virtual bool onTLSConnect(const CertInfo &info);
  virtual void onResourceBind(const std::string &resource);
  virtual void onResourceBindError(const Error *error);
  virtual void onConnect();
  virtual void onDisconnect(ConnectionError e);
  virtual void onSessionCreateError(const Error *error);

  /* ================================================================ */
  /* DiscoHandler                                                     */
  /* XEP-0030: Service Discovery                                      */
  /* ================================================================ */

  virtual void handleDiscoInfo(const JID &from, const Disco::Info &info,
                               int context);
  virtual void handleDiscoItems(const JID &from, const Disco::Items &items,
                                int context);
  virtual void handleDiscoError(const JID &from, const Error *error,
                                int context);
  virtual bool handleDiscoSet(const IQ &iq);

  /* ================================================================ */
  /* XEP-0048 Bookmark Storage                                        */
  /* ================================================================ */

  void handleBookmarks(const BookmarkList &bList, const ConferenceList &cList);

  virtual void handleMessage(const Message &stanza, MessageSession *session);
  virtual void handleMessageEvent(const JID &from, MessageEventType event);
  virtual void handleMessageSession(MessageSession *session);
  virtual void
  handleMUCParticipantPresence(MUCRoom *room,
                               const MUCRoomParticipant participant,
                               const Presence &presence);
  virtual void handleMUCMessage(MUCRoom *room, const Message &msg, bool priv);

  virtual bool handleMUCRoomCreation(MUCRoom *room);

  virtual void handleMUCSubject(MUCRoom *room, const std::string &nick,
                                const std::string &subject);

  virtual void handleMUCInviteDecline(MUCRoom *room, const JID &invitee,
                                      const std::string &reason);

  virtual void handleMUCError(MUCRoom *room, StanzaError error);
  virtual void handleMUCInfo(MUCRoom *room, int features,
                             const std::string &name, const DataForm *infoForm);

  virtual void handleMUCItems(MUCRoom *room, const Disco::ItemList &items);
  virtual void handleLog(LogLevel level, LogArea area,
                         const std::string &message);

private:
  std::string m_user;
  std::string m_host;
  std::list<std::string> m_discoitems;
  Client *j;
};

#endif // XMPP_HAWKBIT_INFO_H___
